**CleanCaptions**

Converts a YouTube captions file (captions.srt) to a transcript (captions.txt)

To run the project, edit captions.srt and replace the contents with your captions. After the commit, the pipeline will run, and you will find captions.txt as an artifact. 