import sys
import os
import re

def	main():
	try:
		# Get command line args
		num_args = len(sys.argv)
		if num_args	< 2:
			print ("Usage: python Captions2Transcript.py caption-file-path")
			return 1
		caption_file_name  = sys.argv[1]
		output_file_name  = caption_file_name.split (".srt")[0] + ".txt"

		caption_file = open(caption_file_name, 'r')
		output_file = open(output_file_name, "w+")
		for line in caption_file:
			line = line.strip()
			# Drop empty lines and lines starting and ending with digits:
			if (line and not re.search("^\d.*\d$", line) and not re.search("^\d$", line)):
				output_file.write(line + "\n")
		caption_file.close()
		output_file.close()

	except Exception as ex:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		print( "Error: " + str(ex))

		return 1 # 
	else:
		return 0 # 


# this is the standard boilerplate that	calls the main()	 function
if __name__	== '__main__':
	# sys.exit(main(sys.argv)) # used to give a	better look	to exists
	main()
